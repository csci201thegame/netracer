Abhishek Biswas     biswasa@usc.edu     29909

Hui-Yu Lien         huiyulie@usc.edu    29909

Lifan Zhao          lifanzha@usc.edu    29909

Sneha Patkar        spatkar@usc.edu     29909

Thoman Finn         thomasjf@usc.edu    29909

**Team Instructions for Unity**

The top-level folder "NetRacer" is the Unity project folder to be opened in the editor. Non-Unity components of the project should be placed in a separate directory.

**IMPORTANT**: When working on your local machine, place any scenes that you are actively editing in a sub-folder with the path **"NetRacer/Assets/Scenes/Test"**. *These scenes will not be added to source control to avoid merge conflicts.*

When you're ready to create a new build, copy your scene(s) into a new folder named **"mm-dd-yy_yourname"**, and place this folder in **"NetRacer/Assets/Scenes/Master Builds"**. *These scenes will be under source control and should be considered working master versions.*

If you wish to add features to an existing build, make a copy of it into your **"NetRacer/Assets/Scenes/Test"** folder and publish it as a new build in **"NetRacer/Assets/Scenes/Master Builds"** when your changes are complete.

**Branching**: You are encouraged to create your own branch for each new feature you work on. Use the naming convention **"mm-dd-yy_featurename_yourname"**. When the feature is complete, pull from master and ensure there are no merge conflicts. Then create a new master build as detailed above, and merge into master. *Please create a **pull request** (or at least discuss with the team) before remerging a branch in order to avoid potential merge conflicts.*

**Unity Asset Store**: DO NOT push Asset Store assets to the repository. If you use them, keep them under the folder "NetRacer/NetRacer/Assets/ThirdParty" to avoid placing them under source control.
