﻿using UnityEngine;
using System.Collections;

public class CollisionDetector : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider other) {

		if (other.gameObject.tag == "finish") {
			Stats s = GetComponentInParent<Stats> ();

			if (s.lapCount < 4) {
				s.IncrementLapCount ();
			}
		}
	}
}
