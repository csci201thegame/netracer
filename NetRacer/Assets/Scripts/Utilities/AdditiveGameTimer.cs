﻿using UnityEngine;
using System.Collections;

public class AdditiveGameTimer : MonoBehaviour {

	// ============================================
	// Instance variables
	// ============================================

	private float gameTime;
	private bool paused;
	private string timerTag;

	// ============================================
	// Game loop
	// ============================================

	void Start () {
		gameTime = 0.0f;
		paused = false;
		timerTag = "";
	}

	void Update () {
		if (!paused)
		{
			gameTime += Time.deltaTime;	
		}
	}

	// ============================================
	// Instance functions
	// ============================================

	public void pauseTimer() {
		paused = true;
	}

	public void unpauseTimer() {
		paused = false;
	}

	public void resetTimer() {
		gameTime = 0.0f;
	}

	// ============================================
	// Public accessors
	// ============================================

	public float getTime() {
		return gameTime;
	}

	public string getTag() {
		return timerTag;
	}

	public void setTag(string tag) {
		timerTag = tag;
	}
}
