﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CustomizeController : MonoBehaviour {

	// ============================================
	// Properties
	// ============================================

	[SerializeField]
	private GameObject vehicleModelHost;

	[SerializeField]
	private GameObject[] vehicleImages;

	[SerializeField]
	private GameObject[] vehicleColors;

	// ============================================
	// Constants
	// ============================================

	// ============================================
	// Local variables
	// ============================================

	private GameObject[] vehicleModels;
	private int currentModelIndex;

	// ============================================
	// Game loop
	// ============================================

	void Start () {
		vehicleModels = vehicleModelHost.GetComponents<GameObject>();
		currentModelIndex = 0;
	}

	void Update () {
	}

	// ============================================
	// Helper functions
	// ============================================

	// Iterate to next model in listing
	void UpdateModel() {
		if (currentModelIndex < vehicleModels.Length) {
			++currentModelIndex;
		} else {
			currentModelIndex = 0;
		}
	}

	void SelectNextVehicle() {
		vehicleModels[currentModelIndex].SetActive(false);
		if (currentModelIndex < vehicleModels.Length) {
			++currentModelIndex;
		} else {
			currentModelIndex = 0;
		}	
	}

	void SelectPreviousVehicle() {
		if (currentModelIndex > 0) {
			--currentModelIndex;
		} else {
			currentModelIndex = vehicleModels.Length - 1;
		}
	}
}
