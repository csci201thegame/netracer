﻿using UnityEngine;
using System.Collections;

// Temp component for testing UI functionality
public class InputTest : MonoBehaviour {

	[SerializeField]
	private GameObject speedLabel;
	[SerializeField]
	private GameObject lapLabel;

	private SpeedLabelController speedController;
	private LapLabelController lapController;

	private float speed;
	private const float SPEED_MULTIPLIER = 3.0f;

	void Start () {
		speed = 0.0f;
		speedController = speedLabel.GetComponent<SpeedLabelController>();
		lapController = lapLabel.GetComponent<LapLabelController>();
	}
	
	// Update is called once per frame
	void Update () {
		// Speed control
		if (Input.GetKey(KeyCode.Space)) {
			speed += Time.deltaTime;
		} else if (speed > 0.0f) {
			speed -= Time.deltaTime;
		}

		if (speedController) {
			speedController.SetInternalSpeed(speed * SPEED_MULTIPLIER);
		}

		// Lap control
		if (lapController && Input.GetKeyDown(KeyCode.L)) {
			lapController.IncrementLap();
		}
	}
}
