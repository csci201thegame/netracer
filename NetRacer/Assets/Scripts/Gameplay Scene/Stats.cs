﻿using UnityEngine;
using System.Collections;

using UnityStandardAssets.Utility;
using UnityEngine.Networking;

using UnityEngine.UI;
using UnityStandardAssets.Cameras;

public class Stats : NetworkBehaviour {
	public int lapCount = 0;
	Text text;
	public RaceLabelController raceLC;
	public LapLabelController lapLC;







	// Use this for initialization
	void Start () {
		NetworkIdentity nIdentity = GetComponent<NetworkIdentity>();
		if (nIdentity.isLocalPlayer) {
			Camera.main.GetComponent<SmoothFollow>().target = transform;
			Camera.main.GetComponent<LookAtTarget>().target = transform;

			raceLC = GameObject.Find("RaceTimeIndexLabel").GetComponent<RaceLabelController> ();
			lapLC = GameObject.Find("LapNumberIndexLabel").GetComponent<LapLabelController> ();
			Debug.Log("New object instanted by me");
		}

		else {
			Debug.Log ("New object instantiated");
		}
	}

	// Update is called once per frame
	void Update () {
		
	
	}
	/*
	void OnNetworkInstantiate(NetworkMessageInfo info) {
		NetworkView nView = GetComponent<NetworkView>();
		/*
		if (nView.isMine) {
			Camera.main.GetComponent<SmoothFollow>().target = transform;
			Debug.Log("New object instanted by me");
		}
				
		else {
			Debug.Log ("New object instantiated by " + info.sender);
		}
				
	}*/

	public void IncrementLapCount () {
		lapLC.IncrementLap ();
	}



}
