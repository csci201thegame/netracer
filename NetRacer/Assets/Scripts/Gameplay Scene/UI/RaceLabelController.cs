﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// ============================================
// Attach to UILabel that tracks time.
// ============================================

public class RaceLabelController : MonoBehaviour {

	// ============================================
	// Constants 
	// ============================================

	private const float MINUTE_TO_SECOND = 60.0f;
	private const float CENTI_TO_SECOND = 0.01f;
	private const string TIME_DELIMITER = " : ";
	private const string BUFFER_CHARACTER = "0";

	// ============================================
	// Instance variables
	// ============================================

	[Tooltip("Race time UILabel with AdditiveGameTimer component.")]
	public GameObject timedObject;

	private AdditiveGameTimer timer;
	private Text gameText;

	// ============================================
	// Game loop 
	// ============================================

	void Start () {
		timer = timedObject.GetComponent<AdditiveGameTimer>();
		gameText = GetComponent<Text>();
	}

	void Update () {
		UpdateText();
	}

	// ============================================
	// Manipulator functions 
	// ============================================

	void UpdateText() {
		// Sanity check
		if (!(timer && gameText)) {
			return;
		}

		gameText.text = ParseTimeComponent(timer.getTime());
	}

	// Use to extract minutes, seconds, milliseconds, etc from current time
	// Calculated time = Sum of times with higher precision
	// Conversion factor the multiplicative factor from seconds to desired unit
	string ParseTimeComponent(float timeInSeconds) {
		// Time components
		string minuteString, secondString, centiString;
		int minuteComp, secondComp, centiComp;

		// Calculate minutes
		minuteComp = (int)(timeInSeconds / MINUTE_TO_SECOND);
		minuteString = minuteComp.ToString();

		// Calculate seconds
		secondComp = (int)(timeInSeconds - (float)(minuteComp * MINUTE_TO_SECOND));
		secondString = secondComp.ToString();
		if (secondComp < 10) {
			secondString = BUFFER_CHARACTER + secondString;
		}

		// Calculate centiseconds
		centiComp = (int)((timeInSeconds - (float)secondComp - (float)(minuteComp * MINUTE_TO_SECOND)) 
								/ CENTI_TO_SECOND);
		centiString = centiComp.ToString();
		if (centiComp < 10) {
			centiString = BUFFER_CHARACTER + centiString;
		}

		string parsedString = minuteString + TIME_DELIMITER + secondString
								+ TIME_DELIMITER + centiString;
		return parsedString;
	}
}
