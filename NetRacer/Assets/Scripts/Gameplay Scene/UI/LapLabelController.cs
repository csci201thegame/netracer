﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// ============================================
// Attach to UILabel that tracks laps.
// Use IncrementLap() on appropriate trigger to 
// increment the lap number.
// ============================================

public class LapLabelController : MonoBehaviour {

	// ============================================
	// Constants 
	// ============================================

	private const string LAP_DELIMITER = " / ";

	// ============================================
	// Instance variables
	// ============================================

	[Tooltip("Race time UILabel with AdditiveGameTimer component.")]
	[SerializeField]
	private GameObject raceTimerObject;

	[Tooltip("Lap time UILabel with AdditiveGameTimer component.")]
	[SerializeField]
	private GameObject lapTimerObject;

	[SerializeField]
	private int maxLaps = 3;

	private AdditiveGameTimer raceTimer;
	private AdditiveGameTimer lapTimer;
	private Text gameText;

	private int currentLaps;

	// ============================================
	// Game loop
	// ============================================

	// Use this for initialization
	void Start () {
		currentLaps = 1;
		raceTimer = raceTimerObject.GetComponent<AdditiveGameTimer>();
		lapTimer = lapTimerObject.GetComponent<AdditiveGameTimer>();
		gameText = GetComponent<Text>();
	}
	
	/*// Update is called once per frame
	void Update () {
		UpdateLap();
	}*/

	// ============================================
	// Manipulator functions
	// ============================================

	void UpdateLap() {
		gameText.text = currentLaps.ToString() + LAP_DELIMITER + maxLaps.ToString();
	}

	// NOTE: Call from external script to increment lap.
	public void IncrementLap() {
		if (currentLaps < maxLaps)	{
			++currentLaps;
			UpdateLap();
			if (lapTimer) {
				lapTimer.resetTimer();
			}
		} else if (currentLaps == maxLaps) {
			if (raceTimer) {
				raceTimer.pauseTimer();
			}
			if (lapTimer) {
				lapTimer.pauseTimer();
			}
		}
	}
}
