﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// ============================================
// Attach to UILabel that tracks vehicle speed.
// Call SetInternalSpeed() on appropriate trigger to 
// change the displayed speed.
// ============================================

public class SpeedLabelController : MonoBehaviour {

	// ============================================
	// Constants 
	// ============================================

	private const string SPEED_UNIT = " mph";

	// ============================================
	// Instance variables
	// ============================================

	private Text speedLabel;


	int internalSpeed;


	// Use this for initialization
	void Start () {
		
	
		speedLabel = GetComponent<Text>();
		internalSpeed = 0;
	}

	// Update is called once per frame
	public void UpdateSpeed (int currentSpeed) {
		internalSpeed = currentSpeed;
		UpdateSpeedText();
	}

	void UpdateSpeedText() {
		speedLabel.text = internalSpeed.ToString() + SPEED_UNIT;
	}

	// NOTE: Call from external script to change speed.
	public void SetInternalSpeed(float speed) {
		internalSpeed = (int)speed;
	}
}
