﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class FinishLine : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		//raceLC = GameObject.Find("RaceTimeIndexLabel").GetComponent<RaceLabelController> ();
		//lapLC = GameObject.Find("LapNumberIndexLabel").GetComponent<LapLabelController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
	if (other.tag.Equals("Player")) {
			NetworkIdentity nIdentity = other.transform.root.gameObject.GetComponent<NetworkIdentity>();
			if (nIdentity.isLocalPlayer) {
				Stats playerStats = other.transform.root.gameObject.GetComponent<Stats>();
				playerStats.IncrementLapCount ();
				Debug.Log ("incremented lap count");	
			}

		}
	}
}
